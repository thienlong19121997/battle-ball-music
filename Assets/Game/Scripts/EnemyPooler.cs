﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooler : MonoBehaviour {

	public static EnemyPooler instance;

	public GameObject[] enemyPrefabs;

	public List<GameObject> enemyList_0 = new List<GameObject>();
	public List<GameObject> enemyList_1 = new List<GameObject>();
	public List<GameObject> enemyList_2 = new List<GameObject>();

	public int enemyIsActive;
	public Transform parentEnemy;

	private int enemyAmount = 40;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		SpawnEnemy ();
	}

	void SpawnEnemy(){
		for (int i = 0; i < enemyPrefabs.Length; i++) {
			for(int j = 0 ; j < enemyAmount;j++){
				GameObject enemyClone = Instantiate (enemyPrefabs [i]) as GameObject;
				enemyClone.transform.SetParent (parentEnemy);
				enemyClone.SetActive (false);
				if (i == 0) {
					enemyList_0.Add (enemyClone);
				} else if (i == 1) {
					enemyList_1.Add (enemyClone);
				} else if (i == 2) {
					enemyList_2.Add (enemyClone);
				}
			}
		}
	}
	
	public GameObject Enemy_0(){
		for (int i = 0; i < enemyList_0.Count; i++) {
			if (enemyList_0 [i].activeInHierarchy == false) {
				enemyList_0 [i].SetActive (true);
				return enemyList_0 [i];
			}
		}

		GameObject enemyClone = Instantiate (enemyPrefabs [0]) as GameObject;
		enemyClone.transform.SetParent (parentEnemy);
		enemyClone.SetActive (true);
		enemyList_0.Add (enemyClone);
		return enemyClone;
	}

	public GameObject Enemy_1(){
		for (int i = 0; i < enemyList_1.Count; i++) {
			if (enemyList_1 [i].activeInHierarchy == false) {
				enemyList_1 [i].SetActive (true);
				return enemyList_1 [i];
			}
		}

		GameObject enemyClone = Instantiate (enemyPrefabs [1]) as GameObject;
		enemyClone.transform.SetParent (parentEnemy);
		enemyClone.SetActive (true);
		enemyList_1.Add (enemyClone);
		return enemyClone;
	}

	public GameObject Enemy_2(){
		for (int i = 0; i < enemyList_2.Count; i++) {
			if (enemyList_2 [i].activeInHierarchy == false) {
				enemyList_2 [i].SetActive (true);
				return enemyList_2 [i];
			}
		}

		GameObject enemyClone = Instantiate (enemyPrefabs [2]) as GameObject;
		enemyClone.transform.SetParent (parentEnemy);
		enemyClone.SetActive (true);
		enemyList_2.Add (enemyClone);
		return enemyClone;
	}

	public void GetEnemyRandom(){
		
		int rand = Random.Range (0, enemyPrefabs.Length);
		switch (rand) {
		case 0:
			Enemy_0 ();
			break;
		case 1:
			Enemy_1 ();
			break;
		case 2:
			Enemy_2 ();
			break;

		}
		enemyIsActive++;
		UIManager.instance.sumEnemyText.text = enemyIsActive.ToString ();
		if (enemyIsActive >= 100) {
			Manager.instance.GameOver ();
		}
	}




}
