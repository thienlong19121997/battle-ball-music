﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyBall : MonoBehaviour {


	public float moveSpeed;
	public float size;
	public float forceShoot;



	private Rigidbody2D rigid;



	void Awake(){
		rigid = GetComponent<Rigidbody2D> ();
	}

	void OnEnable(){

		transform.position = MyBallPooler.instance.startPos.position;

		transform.localScale = new Vector3 (size, size, size);

		Vector2 direction = new Vector2(Random.Range(-1,2),1);

		rigid.AddForce(direction*forceShoot,ForceMode2D.Impulse);

	}

	void FixedUpdate () {
		rigid.velocity = rigid.velocity.normalized * moveSpeed;
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy") {
			float enemySize = col.gameObject.GetComponent<Enemy> ().size;
			if (size >= enemySize) {
				col.gameObject.SetActive (false);
				Manager.instance.KillEnemy ();
			} else {
				gameObject.SetActive (false);
			}
		}
	}


}
