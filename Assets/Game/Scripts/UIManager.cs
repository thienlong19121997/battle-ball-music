﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance;

	[Header(" menu ")]
	public GameObject menuGame;
	public GameObject gameOverMenu;
	public GameObject menuInGame;

	[Header(" text ")]
	public Text sumEnemyText;

	void Awake(){
		instance = this;
	}

	public void GameOver(){
		gameOverMenu.SetActive (true);
	}

	public void BtnPlay(){
		menuInGame.SetActive (true);
		menuGame.SetActive (false);
		Manager.instance.NowRunGame ();
	}
}
