﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {


	public float moveSpeed;
	public float size;




	private Rigidbody2D rigid;



	void Awake(){
		rigid = GetComponent<Rigidbody2D> ();
	}

	void OnEnable(){
		transform.position = Vector2.zero;
		transform.localScale = new Vector3 (size, size, size);
	

		rigid.AddForce(new Vector2(Random.Range(0,400),Random.Range(0,400)),ForceMode2D.Impulse);

	}
		
	void FixedUpdate () {
		rigid.velocity = rigid.velocity.normalized * moveSpeed;
	}





}
