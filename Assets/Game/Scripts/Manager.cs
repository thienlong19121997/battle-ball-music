﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

	public static Manager instance;
	public float spawnPerTime;

	public enum StateGame{
		play,
		gameOver,
		pause,
		menu,

	}

	public StateGame stateGame;

	void Awake(){
		instance = this;
	}


	public void Tap(){
		Debug.Log ("tap");
		// miss
		// return

		// bad
	//	MyBallPooler.instance.MyBall_0();

		// great
	//	MyBallPooler.instance.MyBall_1();

		// prefect
		MyBallPooler.instance.MyBall_2();

	}

	public void NowRunGame(){
		stateGame = StateGame.play;
		SpawnEnemy ();
	}

	public void SpawnEnemy(){
		StartCoroutine (SpawnEnemyOverTime());
	}
	IEnumerator SpawnEnemyOverTime(){
		if (stateGame == StateGame.play) {
			EnemyPooler.instance.GetEnemyRandom ();

			yield return new WaitForSeconds (spawnPerTime);
			StartCoroutine (SpawnEnemyOverTime ());
		}
	}

	public void GameOver(){
		stateGame = StateGame.gameOver;
		UIManager.instance.GameOver ();
	}

	public void KillEnemy(){
		EnemyPooler.instance.enemyIsActive--;
	}
}
