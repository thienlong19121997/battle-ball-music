﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyBallPooler : MonoBehaviour {

	public static MyBallPooler instance;

	public GameObject[] myBallPrefabs;

	public List<GameObject> myBallList_0 = new List<GameObject>();
	public List<GameObject> myBallList_1 = new List<GameObject>();
	public List<GameObject> myBallList_2 = new List<GameObject>();

	public int myBallIsActive;
	public Transform parentMyBall;
	public Transform startPos;

	private int myBallAmount = 20;

	void Awake(){
		instance = this;
	}

	void Start () {
		SpawnEnemy ();
	}

	void SpawnEnemy(){
		for (int i = 0; i < myBallPrefabs.Length; i++) {
			for(int j = 0 ; j < myBallAmount;j++){
				GameObject myBallClone = Instantiate (myBallPrefabs [i]) as GameObject;
				myBallClone.transform.SetParent (parentMyBall);
				myBallClone.SetActive (false);
				if (i == 0) {
					myBallList_0.Add (myBallClone);
				} else if (i == 1) {
					myBallList_1.Add (myBallClone);
				} else if (i == 2) {
					myBallList_2.Add (myBallClone);
				}
			}
		}
	}

	public GameObject MyBall_0(){
		for (int i = 0; i < myBallList_0.Count; i++) {
			if (myBallList_0 [i].activeInHierarchy == false) {
				myBallList_0 [i].SetActive (true);
				return myBallList_0 [i];
			}
		}

		GameObject myBallClone = Instantiate (myBallPrefabs [0]) as GameObject;
		myBallClone.transform.SetParent (parentMyBall);
		myBallClone.SetActive (true);
		myBallClone.transform.position = startPos.position;
		myBallList_0.Add (myBallClone);
		return myBallClone;
	}

	public GameObject MyBall_1(){
		for (int i = 0; i < myBallList_1.Count; i++) {
			if (myBallList_1 [i].activeInHierarchy == false) {
				myBallList_1 [i].SetActive (true);
				return myBallList_1 [i];
			}
		}

		GameObject myBallClone = Instantiate (myBallPrefabs [1]) as GameObject;
		myBallClone.transform.SetParent (parentMyBall);
		myBallClone.SetActive (true);
		myBallClone.transform.position = startPos.position;
		myBallList_1.Add (myBallClone);
		return myBallClone;
	}

	public GameObject MyBall_2(){
		for (int i = 0; i < myBallList_1.Count; i++) {
			if (myBallList_2 [i].activeInHierarchy == false) {
				myBallList_2 [i].SetActive (true);
				return myBallList_2 [i];
			}
		}

		GameObject myBallClone = Instantiate (myBallPrefabs [2]) as GameObject;
		myBallClone.transform.SetParent (parentMyBall);
		myBallClone.SetActive (true);
		myBallClone.transform.position = startPos.position;
		myBallList_2.Add (myBallClone);
		return myBallClone;
	}



}
